/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.hiik.listexample;

import org.testng.Assert;
import org.testng.annotations.Test;
import ru.hiik.entitycore.entity.student.Student;

/**
 *
 * @author vaganovdv
 */
public class InMemoryDatabaseTest
{
    //   тип (класс)      имя экземпляра
    //    |               /
    InMemoryDatabase inMemoryDatabase = new InMemoryDatabase();
    
   
    // 1 тест
    @Test
    public void createInMemoryDatabase()
    {
        //                  проверяемый экземпляр             сообщение об ошибке
        //                       |                               /
        Assert.assertNotNull(inMemoryDatabase,"Ошибка: не удалось создать экземпляр класса {InMemoryDatabase}");   
    }        
    
    // 2 тест 
    @Test //(dependsOnMethods ={"createInMemoryDatabase"})
    public void createStudentList ()
    {
        //                                проверка наличия списка студентов
        //                                       |
        Assert.assertNotNull( inMemoryDatabase.studentList,"Ошибка: список {studentList} не существует");                
    }
     
    
    // 3 тест
     @Test (dependsOnMethods ={"createStudentList"})
     public void checkStudentList()
     {
         // После зануления  списка студентов метод findStudentByLastName вернет значение optional == fasle
         
       
         //                 Вызов метода поиска стуента при отсутвии записей                ожидаемое значение
         //                    /                                                             /
         Assert.assertEquals( inMemoryDatabase.findStudentByLastName("Иванов").isPresent(), false, "Неверное значение функции findStudentByLastName");
     }
    
     // 4 тест
     @Test(dependsOnMethods ={"checkStudentList"})
     public void checkLastNameField()
     {
              
         //                 Вызов метода поиска студента  при отсутвии записей         ожидаемое значение
         //                    /                                                        /
         Assert.assertEquals( inMemoryDatabase.findStudentByLastName("").isPresent(), false, "Неверное значение функции findStudentByLastName");
     }
     
     
    
    // 5 тест
    /**
     * Добавление студента в БД
     */
     @Test (dependsOnMethods ={"checkLastNameField"})
     public void addStudentToList()
     {
        Student student1 = new Student();
        student1.setId(10L);
        student1.setFirstName("Денис"); // Записать FirsName
        student1.setLastName("Петков");
        student1.setMiddleName("Андреевич");
        student1.setYearOfstudy(1);
        
        
        Student student2 = new Student();
        student2.setId(12L);
        student2.setFirstName("Денис"); // Записать FirsName
        student2.setLastName("Иванов");
        student2.setMiddleName("Андреевич");
        student2.setYearOfstudy(1);
        
        
        Student student3 = new Student();
        student3.setId(13L);
        student3.setFirstName("Игорь"); // Записать FirsName
        student3.setLastName("Иванов");
        student3.setMiddleName("Андреевич");
        student3.setYearOfstudy(1);
        
        Student student4 = new Student();
        student4.setId(14L);
        student4.setFirstName("Сергей"); // Записать FirsName
        student4.setLastName("Иванов");
        student4.setMiddleName("Андреевич");
        student4.setYearOfstudy(1);
        
        
        inMemoryDatabase.addStudent(student1);
        inMemoryDatabase.addStudent(student2);
        inMemoryDatabase.addStudent(student3);
        inMemoryDatabase.addStudent(student4);
        // 
        //                      фактичекое значение              ожидаемое значение
        //                          |                             /
        Assert.assertEquals(inMemoryDatabase.studentList.size(), 4 ,"Ошибка: неверный размер списка после добавления студента");       
        
     }       
     
     
     @Test (dependsOnMethods ={"addStudentToList"})
     public void findStudent_Ivanov()
     {
        Assert.assertEquals(inMemoryDatabase.findStudentByLastName("Иванов").isPresent(), true  ,"Ошибка: неверный код выполнения метода");         
        Assert.assertEquals(inMemoryDatabase.findStudentByLastName("Иванов").get().size(), 3  ,"Ошибка: неверное количестов найденных студентов с фамилияей Иванов");         
       
     }        
    
    
    
}
