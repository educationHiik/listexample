/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.hiik.listexample;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import ru.hiik.entitycore.entity.professor.Professor;
import ru.hiik.entitycore.entity.student.Student;

/**
 *
 * @author vaganovdv
 */
public class InMemoryDatabase
{

// 
// список    тип данных списка  экземпляр класса списка  
//   |         /                |
    List   <Student>        studentList   ;     // Список студентов      
    List   <Professor>      professorList ;     // Список преподавателей
    
    
    public InMemoryDatabase()
    {
        studentList   = new ArrayList<Student>(); 
        professorList = new ArrayList<Professor>(); 
    }
    
    /**
     * Добавление студента в список 
     * @param student  Студент, добавляемый в список
     * @return  true если студент добавлен, fasle  в других случаях
     */
    
    public boolean addStudent(Student student)
    {
        
        boolean result =   studentList.add(student);
        if (result)
        {
            System.out.println("Добавлен студент: "+student.toString());
        }    
        System.out.println("Размер списка: "+studentList.size()); // Демонстрация метода для получения размера списка
        
        printStudentList("студентов после добавления", this.studentList);
        
        return result;
    }        
 
    /**
     * Поиск списка студентов с указанной фамилией
     * 
     * @return если Optional<List<Student>>  present  то  функция выполнилась, 
     * 
     */
    public Optional<List<Student>>  findStudentByLastName(String lastName)
    {
    
        // Присвоение opt пустого значения // isPresent == false
        Optional<List<Student>> opt = Optional.empty(); // isPresent == false
        
        List<Student> foundList = new ArrayList<>();
        
        //   Список studentList должен быть не пустым и 
        //   содержать количесво элементов > 0
        //
        if (this.studentList != null && studentList.size() > 0)
        {  
            //  Поле фамилии не нелевое и  не пустое
            if (lastName != null && !lastName.isEmpty())
            {
                // Кода поиска по списку
                
              foundList = studentList
                        // Преобразование в поток
                        .stream()
                        // Фильтрация потока - выбрать все элементы потока в которых   student.getLastName() == lastName   
                        //
                        .filter ( student -> student.getLastName().equalsIgnoreCase(lastName))
                        .collect(Collectors.toList());
                
              System.out.println("Найдено ["+foundList.size()+"] записей  в базе студентов");
              
              if (foundList.size()>0) opt = Optional.of(foundList);
              
            }
        } 
        printStudentList("студентов после операции поиска",foundList );
        
        return opt;
    }        
    
    /**
     * Печать списка  студентов 
     */
    public void printStudentList( String listName, List<Student> studentList)
    {
        
        if (studentList != null && !studentList.isEmpty())
        {
            System.out.println("Список "+listName);
            
            studentList
                    // Преобразование в поток
                    .stream()
                    .forEach // Выполнение действия в { } для каждого 
                    ( 
                        //  переменная метода     тело функции для работы с переменной  
                        //    |                        /
                         student->   {  System.out.println(student.toString()); }
                    );
            
        }    
        
    }        
    
    
    
}
